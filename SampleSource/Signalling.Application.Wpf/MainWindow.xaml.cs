﻿#pragma warning disable 169

//#define THE_OLD_WAY

using IDI.Business.Constants;
using IDI.Common;

using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace Signalling.WpfApp
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	// ReSharper disable once InheritdocConsiderUsage
	// ReSharper disable once RedundantExtendsListEntry
	public partial class MainWindow : Window
	{
		/// <summary>
		/// Mutex to signal that the application is closing, restart the application again after exit
		/// </summary>
		private readonly SignalProcess terminate;

		/// <summary>
		/// initialization/signal thread
		/// </summary>
		// ReSharper disable once PrivateFieldCanBeConvertedToLocalVariable
		private readonly Thread signalThread;

		/// <summary>
		/// Event used to signal mutex object
		/// </summary>
		private readonly ManualResetEvent signalMutex = new ManualResetEvent(false);

		/// <inheritdoc />
		/// <summary>
		/// Ctor
		/// </summary>
		public MainWindow()
		{
			// GUI initialization
			InitializeComponent();

			// initialize signalling object
			terminate = new SignalProcess(AppConstants.UniqueID_Terminate);

#if THE_OLD_WAY
			//
			// Create transmit worker thread and start it.
			// 
			signalThread = new Thread(SignalProc)
			{
				Name = $"{nameof(SignalProc)}{nameof(Thread)}",
				Priority = ThreadPriority.Normal
			};
			signalThread.Start();
#else
			terminate.InitAndWait();
#endif
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void UIThreadSignal_OnClick(object sender, RoutedEventArgs e)
		{
#if THE_OLD_WAY
			signalMutex.Set();
#else
			terminate.Signal();
#endif
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void BackgroundThreadSignal_OnClick(object sender, RoutedEventArgs e)
		{
#if THE_OLD_WAY
			Task.Factory.StartNew(() => signalMutex.Set());
#else
			Task.Factory.StartNew(() => terminate.Signal());
#endif
		}

		/// <summary>
		/// Old way of doing things - spawn thread proc to create and signal mutex
		/// </summary>
		// ReSharper disable once UnusedMember.Local
		private void SignalProc()
		{
			// create mutext object
			terminate.Init(true);

			// camp on event object -- wait here until we are ready to signal mutex
			signalMutex.WaitOne();

			// release mutex
			terminate.Release();

			// in case we want to close the application
			//System.Windows.Application.Current.Dispatcher.Invoke(Close);
		}
	}
}
