﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Common
{
    /// <summary>
    /// Implementation of <see cref="INotifyPropertyChanged" /> to simplify models.
    /// </summary>
    [Serializable]
    // ReSharper disable once InheritdocConsiderUsage
    public abstract class BindableBase : INotifyPropertyChanged
    {
        /// <inheritdoc />
        /// <summary>
        /// Ctor
        /// </summary>
        protected BindableBase()
        {
        }

        /// <inheritdoc />
        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="name"></param>
        protected BindableBase(string name)
        {

        }

        /// <inheritdoc />
        /// <summary>
        /// Multi-cast event for property change notifications.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Checks if a property already matches a desired value.  Sets the property and
        /// notifies listeners only when necessary.
        /// </summary>
        /// <typeparam name="T">Type of the property.</typeparam>
        /// <param name="storage">Reference to a property with both getter and setter.</param>
        /// <param name="value">Desired value for the property.</param>
        /// <param name="propertyName">
        /// Name of the property used to notify listeners.  This
        /// value is optional and can be provided automatically when invoked from compilers that
        /// support CallerMemberName.
        /// </param>
        /// <returns>
        /// True if the value was changed, false if the existing value matched the
        /// desired value.
        /// </returns>
        // ReSharper disable once UnusedMethodReturnValue.Global
        protected bool SetProperty<T>(ref T storage, T value, [CallerMemberName] string propertyName = null)
        {
            // do nothing if we are setting the same value
            if (Equals(storage, value))
            {
                return false;
            }

            // store new value
            storage = value;

            // notify caller
            if (propertyName != null)
            {
                OnPropertyChanged(propertyName);
            }

            // all is well
            return true;
        }

        /// <summary>
        /// Notifies listeners that a property value has changed.
        /// </summary>
        /// <param name="propertyName">
        /// Name of the property used to notify listeners.  This
        /// value is optional and can be provided automatically when invoked from compilers
        /// that support <see cref="CallerMemberNameAttribute" />.
        /// </param>
        public void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
