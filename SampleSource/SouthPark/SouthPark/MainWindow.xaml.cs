﻿using DataLayer;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace SouthPark
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    // ReSharper disable once UnusedMember.Global
    // ReSharper disable once RedundantExtendsListEntry
    public partial class MainWindow : Window
    {
        //private readonly CharacterCollectionModel characterCollection = new CharacterCollectionModel();
        private readonly MainWindowModel model = new MainWindowModel();
        private DispatcherTimer timer;

        public MainWindow()
        {
            // initial GUI start
            InitializeComponent();

            // bindings
            ICharacterFactory factory = new CharacterFactory();

            // uncomment this line to only show the 4 male characters
            //factory.Populate(model.CharacterCollection, c => c.Gender == Gender.Male);

            // uncomment this line to show all 5 characters including Wendy
            factory.Populate(model.CharacterCollection, c => true);

            DataContext = model;

            // "real" time clock
            timer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(1) };
            timer.Tick += (sender, args) => { model.TimeStamp = DateTime.Now.ToString("G");};
            timer.Start();
        }

        private void SouthParkCharacterList_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // handle case of removals -- ignore 'AddItems' in this case
            var added = e.AddedItems;
            if (added.Count < 1)
            {
                return;
            }

            // otherwise we have valid character info block, update info on the left
            var c = e.AddedItems[0] as ICharacter;
            model.UpdateCharacter(c);
        }

        private void Add_OnClick(object sender, RoutedEventArgs e)
        {
            // create an empty character model when adding a new character
            var details = new CharacterDetails(new Character());
            var results = details.ShowDialog();

            // update our list if 'Save' was selected
            if (results.GetValueOrDefault())
            {
                model.CharacterCollection.Add(details.Character);
                model.UpdateCharacter(details.Character);
            }
        }

        private void EditButton_OnClick(object sender, RoutedEventArgs e)
        {
            // retrieve the selected character
            var selIndex = SouthParkCharacterList.SelectedIndex;
            var character = model.CharacterCollection[selIndex];

            // update the editor window
            var details = new CharacterDetails(character);
            var results = details.ShowDialog();

            // update our list if 'Save' was selected
            if (results.GetValueOrDefault())
            {
                model.CharacterCollection[selIndex] = details.Character;
                model.UpdateCharacter(details.Character);
            }
        }

        private void RemoveButton_OnClick(object sender, RoutedEventArgs e)
        {
            // remove character from collection
            var selIndex = SouthParkCharacterList.SelectedIndex;
            model.CharacterCollection.RemoveAt(selIndex);

            // adjust listbox selection -- implicitly updates info on left
            if (selIndex >= SouthParkCharacterList.Items.Count)
            {
                selIndex = SouthParkCharacterList.Items.Count - 1;
            }
            SouthParkCharacterList.SelectedIndex = selIndex;

            // clear info on left if no more characters
            if (selIndex < 0)
            {
                model.ClearCharacter();
            }
        }
    }
}
