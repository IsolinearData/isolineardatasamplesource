﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace SouthPark
{
    /// <summary>
    /// Purpose:
    /// 
    /// To convert a DrinkTypeCode to its corresponding image icon to display. Additional support has been added to compensate for specification
    /// of which assembly and subfolder the images reside in.
    /// </summary>
    [ValueConversion(typeof(string), typeof(string))]
    public sealed class CharacterToImagePathConverter : IValueConverter
    {
        #region Conversion

        /// <inheritdoc />
        /// <summary>
        /// Converts from the binding to the target
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // sanity check
            if (!(value is string))
            {
                return null;
            }

            // return result
            return $"../../Resources/{value}.png";
        }

        /// <inheritdoc />
        /// <summary>
        /// Converts from the target to the binding
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }

        #endregion
    }
}
