﻿using System;
using System.Linq;
using System.Windows.Data;

namespace SouthPark
{
    class IsNotEmptyOrNullOrUnselectedValueConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values == null)
                return false;

            return values.All(c => c is string ? !string.IsNullOrEmpty((string)c) : c != null);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
