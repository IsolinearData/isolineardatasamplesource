﻿using DataLayer;
using Microsoft.Win32;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;

namespace SouthPark
{
    /// <summary>
    /// Interaction logic for CharacterDetails.xaml
    /// </summary>
    public partial class CharacterDetails : Window
    {
        private readonly Character character;
        public Character Character => character;

        public CharacterDetails(ICharacter c)
        {
            // GUI initialize
            InitializeComponent();

            var genderVals = Enum.GetValues(typeof(Gender)).Cast<Gender>().Where(g => g != Gender.Invalid).ToList();
            GenderVals.ItemsSource = genderVals;

            // data bind
            character = new Character(c);
            DataContext = character;
        }

        private void SelectImage_OnClick(object sender, RoutedEventArgs e)
        {
            var defPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase).Replace(@"file:\", "");
            var resPath = $@"{defPath}\..\..\Resources\";
            var absolute = Path.GetFullPath(resPath);

            // Create OpenFileDialog
            var dlg = new OpenFileDialog
            {
                DefaultExt = ".png",
                Filter = "Image Files (.png)|*.png",
                InitialDirectory = absolute
            };

            // Display OpenFileDialog by calling ShowDialog method
            var result = dlg.ShowDialog();

            // Get the selected file name and display in a TextBox
            if (result == true)
            {
                // update character image
                character.Picture = dlg.FileName;
            }
        }

        private void Save_OnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void Cancel_OnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
