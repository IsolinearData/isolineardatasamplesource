﻿using System;
using Common;
using DataLayer;

namespace SouthPark
{
    public class MainWindowModel : BindableBase
    {
        private readonly Character character = new Character();
        public Character SelectedCharacter => character;

        private readonly CharacterCollectionModel characterCollection = new CharacterCollectionModel();
        public CharacterCollectionModel CharacterCollection => characterCollection;

        private string timestamp;
        public string TimeStamp
        {
            get { return timestamp; }
            set { SetProperty(ref timestamp, value); }
        }

        public void UpdateCharacter(ICharacter c)
        {
            if (c == null)
            {
                throw new ArgumentNullException(nameof(c));
            }

            character.Picture = c.Picture;
            character.FirstName = c.FirstName;
            character.LastName = c.LastName;
            character.NickName = c.NickName;
            character.Gender = c.Gender;
        }

        public void ClearCharacter()
        {
            character.Picture = string.Empty;
            character.FirstName = string.Empty;
            character.LastName = string.Empty;
            character.NickName = string.Empty;
            character.Gender = Gender.Invalid;

        }
    }
}
