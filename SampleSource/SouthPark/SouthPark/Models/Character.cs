﻿using System;
using Common;
using DataLayer;

namespace SouthPark
{
    public class Character : BindableBase, ICharacter
    {
        public Character()
        {
            Picture = string.Empty;
            FirstName = string.Empty;
            LastName = string.Empty;
            NickName = string.Empty;
            Gender = Gender.Invalid;
        }

        public Character(ICharacter c)
        {
            Picture = c.Picture;
            FirstName = c.FirstName;
            LastName = c.LastName;
            NickName = c.NickName;
            Gender = c.Gender;
        }

        private string picture;
        public string Picture
        {
            get { return picture; }
            set { SetProperty(ref picture, value); }
        }

        private string firstName;
        public string FirstName
        {
            get { return firstName;}
            set { SetProperty(ref firstName, value); }
        }

        private string lastName;
        public string LastName
        {
            get { return lastName; }
            set { SetProperty(ref lastName, value); }
        }

        private string nickName;
        public string NickName
        {
            get { return nickName; }
            set { SetProperty(ref nickName, value); }
        }

        private Gender gender;
        public Gender Gender
        {
            get { return gender; }
            set { SetProperty(ref gender, value); }
        }
    }
}
