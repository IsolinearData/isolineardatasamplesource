﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace DataLayer
{
    public class CharacterFactory : ICharacterFactory
    {
        private static Character[] characters = 
        {
            new Character { Picture = "Eric.png", FirstName = "Eric", LastName = "Cartman", Gender = Gender.Male, NickName = "Eric"},
            new Character { Picture = "Kenny.png", FirstName = "Kenny", LastName = "McCormick", Gender = Gender.Male, NickName = "Kenny" },
            new Character { Picture = "Kyle.png", FirstName = "Kyle", LastName = "Broflovsky", Gender = Gender.Male, NickName = "Kyle" },
            new Character { Picture = "Stan.png", FirstName = "Stan", LastName = "Marsh", Gender = Gender.Male, NickName = "Stan" },
            new Character { Picture = "", FirstName = "Wendy", LastName = "Testaburger ", Gender = Gender.Female, NickName = "Wendy" }
        };

        public void Populate(CharacterCollectionModel charCollection, Predicate<ICharacter> filter)
        {
            if (charCollection == null)
            {
                throw new ArgumentNullException(nameof(charCollection));
            }

            var defPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase).Replace(@"file:\", "");
            var resPath = $@"{defPath}\..\..\Resources";

            characters.ToList().ForEach(character =>
            {
                if (filter(character))
                {
                    if (character.Picture.Length > 0)
                    {
                        character.Picture = $@"{resPath}\{character.Picture}";
                    }
                    charCollection.Add(character);
                }
            });
        }
    }
}
