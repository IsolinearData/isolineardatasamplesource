﻿namespace DataLayer
{
    public enum Gender
    {
        Invalid = -1,
        Male,
        Female
    }
}
