﻿using System;

namespace DataLayer
{
    public interface ICharacterFactory
    {
        void Populate(CharacterCollectionModel charCollection, Predicate<ICharacter> filter);
    }
}
