﻿namespace DataLayer
{
    public interface ICharacter
    {
        string Picture { get; set; }

        string FirstName { get; set; }

        string LastName { get; set; }

        string NickName { get; set; }

        Gender Gender { get; set; }
    }
}
