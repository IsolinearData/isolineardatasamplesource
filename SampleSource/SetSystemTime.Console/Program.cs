﻿using IDI.Common;

using PInvoke.Net;

using System;

namespace SetSystemTime.Console
{
	internal static class Program
	{
		private static void Main(string[] args)
		{
			// convert DateTime -> SystemTime
			var timeFromGui = DateTime.Now;
			var systemTime = new SystemTime(timeFromGui);

			// save to OS -> hardware
			LsaUtility.SetRight("admin", "SeSystemtimePrivilege");

			PrivilegeHelper.EnablePrivilege(SecurityEntity.SE_SYSTEMTIME_NAME);
			PrivilegeHelper.EnablePrivilege(SecurityEntity.SE_TIME_ZONE_NAME);

			var result = Kernel32.SetSystemTime(ref systemTime);
			if (!result)
			{
				var rc = Kernel32.GetLastError();
			}
		}
	}
}
