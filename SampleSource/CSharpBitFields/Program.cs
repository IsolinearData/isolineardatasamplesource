﻿using System;
using System.Collections.Specialized;

// ReSharper disable UnusedMember.Global
// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable
// ReSharper disable UnusedParameter.Local
// ReSharper disable RedundantEmptyObjectOrCollectionInitializer
// ReSharper disable UnusedMember.Local

// ReSharper disable FieldCanBeMadeReadOnly.Global
// ReSharper disable MemberCanBePrivate.Global

namespace CSharpBitFields
{
	internal static class Program
	{
		public class Foo
		{
			public int Beep { get; set; }
		}

		/// <summary>
		/// Main entry point
		/// </summary>
		/// <param name="args"></param>
		static void Main(string[] args)
		{
			var myUnion1 = new MyUnion(0)
			{
				Flag2 = true
			};

			var f = new Foo();
			Console.WriteLine(f.Beep++);

			Console.WriteLine(f.Beep++);



			var myUnion2 = new MyUnion(0)
			{
				Flag1 = true,
				Flag3 = true
			};

			Console.WriteLine(myUnion1);
			Console.WriteLine(myUnion2);

			var myUnion3 = new MyStaticUnion(0)
			{
				Flag2 = true
			};

			var myUnion4 = new MyStaticUnion(0)
			{
				Flag1 = true,
				Flag3 = true
			};

			Console.WriteLine(myUnion3);
			Console.WriteLine(myUnion4);

			Console.WriteLine(myUnion3);

			// ReSharper disable once UnusedVariable
			var rgb16 = new Rgb16(0)
			{
				R = 127,
				G = 99,
				B = 100
			};

			//TestBitVector32();

			var foo = new AbsPosDrnkBytes(0)
			{
				//Flavor = 1,
				//Ice = 2,
				//Size = 4
			};

			foo.Drnk0 = 149;
			foo.Drnk1 = 195;
			foo.Drnk2 = 128;
			
			// ReSharper disable once UnusedVariable
			var rgb16s = new Rgb16(0)
			{
				R = 127,
				G = 99,
				B = 100
			};

			Console.WriteLine(foo.Drnk0);
		}

		private static void TestBitVector32()
		{
			// Creates and initializes a BitVector32.
			BitVector32 myBV = new BitVector32(0);

			// Creates four sections in the BitVector32 with maximum values 6, 3, 1, and 15.
			// mySect3, which uses exactly one bit, can also be used as a bit flag.
			BitVector32.Section mySect1 = BitVector32.CreateSection(6);
			BitVector32.Section mySect2 = BitVector32.CreateSection(3, mySect1);
			BitVector32.Section mySect3 = BitVector32.CreateSection(1, mySect2);
			BitVector32.Section mySect4 = BitVector32.CreateSection(15, mySect3);

			// Displays the values of the sections.
			Console.WriteLine("Initial values:");
			Console.WriteLine("\tmySect1: {0}", myBV[mySect1]);
			Console.WriteLine("\tmySect2: {0}", myBV[mySect2]);
			Console.WriteLine("\tmySect3: {0}", myBV[mySect3]);
			Console.WriteLine("\tmySect4: {0}", myBV[mySect4]);

			// Sets each section to a new value and displays the value of the BitVector32 at each step.
			Console.WriteLine("Changing the values of each section:");
			Console.WriteLine("\tInitial:    \t{0}", myBV.ToString());
			myBV[mySect1] = 5;
			Console.WriteLine(myBV[mySect1]);
			Console.WriteLine("\tmySect1 = 5:\t{0}", myBV.ToString());
			myBV[mySect2] = 3;
			Console.WriteLine(myBV[mySect2]);
			Console.WriteLine("\tmySect2 = 3:\t{0}", myBV.ToString());
			myBV[mySect3] = 1;
			Console.WriteLine(myBV[mySect3]);
			Console.WriteLine("\tmySect3 = 1:\t{0}", myBV.ToString());
			myBV[mySect4] = 9;
			Console.WriteLine("\tmySect4 = 9:\t{0}", myBV.ToString());
			Console.WriteLine(myBV[mySect4]);

			// Displays the values of the sections.
			Console.WriteLine("New values:");
			Console.WriteLine("\tmySect1: {0}", myBV[mySect1]);
			Console.WriteLine("\tmySect2: {0}", myBV[mySect2]);
			Console.WriteLine("\tmySect3: {0}", myBV[mySect3]);
			Console.WriteLine("\tmySect4: {0}", myBV[mySect4]);
		}
	}
}
