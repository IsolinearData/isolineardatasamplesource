﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace CSharpBitFields
{
	[StructLayout(LayoutKind.Explicit, Size = 1, CharSet = CharSet.Ansi)]
	public struct AbsPosDrnkBytes
	{
		#region Lifetime

		/// <summary>
		/// Cant define a default Ctor for a struct, have to fake it out with a param
		/// Note: I tried a default param such as "int foo = 0", it doesn't always behave the way
		/// you would expect...
		/// </summary>
		/// <param name="foo">ignored, don't</param>
		// ReSharper disable once UnusedParameter.Local
		public AbsPosDrnkBytes(int foo)
		{
			// allocate the bitfield
			drnk = new BitVector32(0);

			// Next, allocate the sections and chain them, making sure to
			// specify the correct number/size of the bitmask for each bitmask...

			// brand and ice
			flavor = BitVector32.CreateSection(0x0f);
			ice = BitVector32.CreateSection(0x07, flavor);

			// always 1
			always1_0 = BitVector32.CreateSection(0x01, ice);

			// cup size and lid
			sizeStd = BitVector32.CreateSection(0x03, always1_0);
			lid = BitVector32.CreateSection(0x01, sizeStd);
			sizeEnh = BitVector32.CreateSection(0x01, lid);

			notUsed = BitVector32.CreateSection(0x03, sizeEnh);
			drinkId0_1 = BitVector32.CreateSection(0x03, notUsed);

			// always 1
			always1_1 = BitVector32.CreateSection(0x01, drinkId0_1);

			drinkId0_2 = BitVector32.CreateSection(0x7f, always1_1);

			// always 1
			always1_2 = BitVector32.CreateSection(0x01, drinkId0_2);

			// initialize the properties...
			Drnk0 = 0;
			Drnk1 = 0;
			Drnk2 = 0;
		}

		#endregion

		#region Serial fields

		[FieldOffset(0)]
		public byte Drnk0;

		[FieldOffset(1)]
		public byte Drnk1;

		[FieldOffset(2)]
		public byte Drnk2;

		#endregion

		#region Bifield

		// Creates and initializes a BitVector32.
		[FieldOffset(0)]
		private BitVector32 drnk;

		#endregion

		#region Bitfield sections

		[FieldOffset(4)]
		private readonly BitVector32.Section flavor;

		[FieldOffset(8)]
		private readonly BitVector32.Section ice;

		[FieldOffset(12)]
		private readonly BitVector32.Section always1_0;

		[FieldOffset(16)]
		private readonly BitVector32.Section sizeStd;

		[FieldOffset(20)]
		private readonly BitVector32.Section lid;

		[FieldOffset(24)]
		private readonly BitVector32.Section sizeEnh;

		[FieldOffset(28)]
		private readonly BitVector32.Section notUsed;

		[FieldOffset(32)]
		private readonly BitVector32.Section drinkId0_1;

		[FieldOffset(36)]
		private readonly BitVector32.Section always1_1;

		[FieldOffset(40)]
		private readonly BitVector32.Section drinkId0_2;

		[FieldOffset(44)]
		private readonly BitVector32.Section always1_2;

		#endregion

		#region Properties

		/// <summary>
		/// 
		/// </summary>
		public byte Flavor
		{
			get { return (byte)drnk[flavor]; }
			set { drnk[flavor] = value; }
		}

		/// <summary>
		/// 
		/// </summary>
		public byte Ice
		{
			get { return (byte)drnk[ice]; }
			set { drnk[ice] = value; }
		}

		/// <summary>
		/// 
		/// </summary>
		public byte Size
		{
			get
			{
				var size = (byte)(drnk[sizeStd] | drnk[sizeEnh] << 2);
				return size;
			}
			set
			{
				var lsb = value & 0x03;
				drnk[sizeStd] = lsb;
				var msb = (value & 0x07) >> 2;
				drnk[sizeEnh] = msb;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public bool Lid
		{
			get { return drnk[lid] != 0; }
			set { drnk[lid] = value ? 1 : 0; }
		}

		#endregion
	}
}
