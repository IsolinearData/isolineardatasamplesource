﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace AsyncAwait
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	// ReSharper disable once RedundantExtendsListEntry
	// ReSharper disable once InheritdocConsiderUsage
	// ReSharper disable once UnusedMember.Global
	public partial class MainWindow : Window
	{
		/// <summary>
		/// Ctor
		/// </summary>
		public MainWindow()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Start a long series of asynchronous tasks using the Dispatcher for coordinating
		/// UI updates.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Start_Via_Dispatcher_OnClick(object sender, RoutedEventArgs e)
		{
			// update initial start time and task status
			Time_Dispatcher.Text = DateTime.Now.ToString("hh:mm:ss");
			Status_Dispatcher.Text = "Started";

			// create UI dont event object
			var uiUpdateDone = new ManualResetEvent(false);

			// Start a new task (this uses the default TaskScheduler, 
			// so it will run on a ThreadPool thread).
			Task.Factory.StartNew(async () =>
			{
				// We are running on a ThreadPool thread here.

				// Do some work.
				await Task.Delay(2000);

				// Report progress to the UI.
				Application.Current.Dispatcher.Invoke(() =>
				{
					Time_Dispatcher.Text = DateTime.Now.ToString("hh:mm:ss");

					// signal that update is complete
					uiUpdateDone.Set();
				});

				// wait for UI thread to complete and reset event object
				uiUpdateDone.WaitOne();
				uiUpdateDone.Reset();

				// Do some work.
				await Task.Delay(2000); // Do some work.

				// Report progress to the UI.
				Application.Current.Dispatcher.Invoke(() =>
				{
					Time_Dispatcher.Text = DateTime.Now.ToString("hh:mm:ss");

					// signal that update is complete
					uiUpdateDone.Set();
				});

				// wait for UI thread to complete and reset event object
				uiUpdateDone.WaitOne();
				uiUpdateDone.Reset();

				// Do some work.
				await Task.Delay(2000); // Do some work.

				// Report progress to the UI.
				Application.Current.Dispatcher.Invoke(() =>
				{
					Time_Dispatcher.Text = DateTime.Now.ToString("hh:mm:ss");

					// signal that update is complete
					uiUpdateDone.Set();
				});

				// wait for UI thread to complete and reset event object
				uiUpdateDone.WaitOne();
				uiUpdateDone.Reset();
			},
			CancellationToken.None,
			TaskCreationOptions.None,
			TaskScheduler.Default)
				.ConfigureAwait(false)
				.GetAwaiter()
				.GetResult()
				.ContinueWith(_ =>
				{
					Application.Current.Dispatcher.Invoke(() =>
					{
						Status_Dispatcher.Text = "Finished";

						// dispose of event object
						uiUpdateDone.Dispose();
					});
				});
		}

		/// <summary>
		/// Start a long series of asynchronous tasks using the task scheduler object for
		/// coordinating UI updates.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Start_Via_TaskScheduler_OnClick(object sender, RoutedEventArgs e)
		{
			Time_TaskScheduler.Text = DateTime.Now.ToString("hh:mm:ss");


			// This TaskScheduler captures SynchronizationContext.Current.
			var taskScheduler = TaskScheduler.FromCurrentSynchronizationContext();
			Status_TaskScheduler.Text = "Started";

			// Start a new task (this uses the default TaskScheduler, 
			// so it will run on a ThreadPool thread).
			Task.Factory.StartNew(async () =>
			{
				// We are running on a ThreadPool thread here.

				// Do some work.
				await Task.Delay(2000);

				// Report progress to the UI.
				var reportProgressTask = ReportProgressTask(taskScheduler, () =>
				{
					Time_TaskScheduler.Text = DateTime.Now.ToString("hh:mm:ss");
					return 90;
				});

				// get result from UI thread
				var result = reportProgressTask.Result;
				Debug.WriteLine(result);

				// Do some work.
				await Task.Delay(2000); // Do some work.

				// Report progress to the UI.
				reportProgressTask = ReportProgressTask(taskScheduler, () =>
					{
						Time_TaskScheduler.Text = DateTime.Now.ToString("hh:mm:ss");
						return 10;
					});

				// get result from UI thread
				result = reportProgressTask.Result;
				Debug.WriteLine(result);

				// Do some work.
				await Task.Delay(2000); // Do some work.

				// Report progress to the UI.
				reportProgressTask = ReportProgressTask(taskScheduler, () =>
				{
					Time_TaskScheduler.Text = DateTime.Now.ToString("hh:mm:ss");
					return 340;
				});

				// get result from UI thread
				result = reportProgressTask.Result;
				Debug.WriteLine(result);
			}, 
			CancellationToken.None,
			TaskCreationOptions.None,
			TaskScheduler.Default)
				.ConfigureAwait(false)
				.GetAwaiter()
				.GetResult()
				.ContinueWith(_ =>
				{
					var reportProgressTask = ReportProgressTask(taskScheduler, () =>
					{
						Status_TaskScheduler.Text = "Finished";
						return 0;
					});
					reportProgressTask.Wait();
				});
		}

		/// <summary>
		/// Helper for creating a UI update task
		/// </summary>
		/// <param name="taskScheduler"></param>
		/// <param name="func"></param>
		/// <returns></returns>
		private Task<int> ReportProgressTask(TaskScheduler taskScheduler, Func<int> func)
		{
			var reportProgressTask = Task.Factory.StartNew(func,
				CancellationToken.None,
				TaskCreationOptions.None,
				taskScheduler);
			return reportProgressTask;
		}

		/// <summary>
		/// Start a long series of asynchronous tasks using the SynchronizationContext object for
		/// coordinating UI updates.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Start_Via_SynchronizationContext_OnClick(object sender, RoutedEventArgs e)
		{
			// update initial time and task status
			Time_SynchronizationContext.Text = DateTime.Now.ToString("hh:mm:ss");
			Status_SynchronizationContext.Text = "Started";

			// capture synchronization context
			var sc = SynchronizationContext.Current;

			// Start a new task (this uses the default TaskScheduler, 
			// so it will run on a ThreadPool thread).
			Task.Factory.StartNew(async () =>
			{
				// We are running on a ThreadPool thread here.

				// Do some work.
				await Task.Delay(2000);

				// Report progress to the UI.
				sc.Send(state =>
				{
					Time_SynchronizationContext.Text = DateTime.Now.ToString("hh:mm:ss");
				}, null);

				// Do some work.
				await Task.Delay(2000);

				// Report progress to the UI.
				sc.Send(state =>
				{
					Time_SynchronizationContext.Text = DateTime.Now.ToString("hh:mm:ss");
				}, null);

				// Do some work.
				await Task.Delay(2000);

				// Report progress to the UI.
				sc.Send(state =>
				{
					Time_SynchronizationContext.Text = DateTime.Now.ToString("hh:mm:ss");
				}, null);
			},
			CancellationToken.None,
			TaskCreationOptions.None,
			TaskScheduler.Default)
			.ConfigureAwait(false)
			.GetAwaiter()
			.GetResult()
			.ContinueWith(_ =>
			{
				sc.Post(state =>
				{
					Status_SynchronizationContext.Text = "Finished";
				}, null);
			});
		}
	}
}
