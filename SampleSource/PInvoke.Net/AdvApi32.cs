﻿using System;
using System.Runtime.InteropServices;
using System.Text;
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable FieldCanBeMadeReadOnly.Global

// ReSharper disable MemberCanBePrivate.Local

namespace PInvoke.Net
{
	public static class AdvApi32
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="SystemName"></param>
		/// <param name="ObjectAttributes"></param>
		/// <param name="DesiredAccess"></param>
		/// <param name="PolicyHandle"></param>
		/// <returns></returns>
		[DllImport("advapi32.dll", PreserveSig = true)]
		public static extern UInt32 LsaOpenPolicy(
			ref LSA_UNICODE_STRING SystemName,
			ref LSA_OBJECT_ATTRIBUTES ObjectAttributes,
			Int32 DesiredAccess,
			out IntPtr PolicyHandle
		);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="PolicyHandle"></param>
		/// <param name="AccountSid"></param>
		/// <param name="UserRights"></param>
		/// <param name="CountOfRights"></param>
		/// <returns></returns>
		[DllImport("advapi32.dll", SetLastError = true, PreserveSig = true)]
		public static extern uint LsaAddAccountRights(
			IntPtr PolicyHandle,
			IntPtr AccountSid,
			LSA_UNICODE_STRING[] UserRights,
			uint CountOfRights);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pSid"></param>
		[DllImport("advapi32")]
		public static extern void FreeSid(IntPtr pSid);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="lpSystemName"></param>
		/// <param name="lpAccountName"></param>
		/// <param name="psid"></param>
		/// <param name="cbsid"></param>
		/// <param name="domainName"></param>
		/// <param name="cbdomainLength"></param>
		/// <param name="use"></param>
		/// <returns></returns>
		[DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true, PreserveSig = true)]
		public static extern bool LookupAccountName(
			string lpSystemName, string lpAccountName,
			IntPtr psid,
			ref int cbsid,
			StringBuilder domainName,
			ref int cbdomainLength,
			ref int use);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pSid"></param>
		/// <returns></returns>
		[DllImport("advapi32.dll")]
		public static extern bool IsValidSid(IntPtr pSid);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="ObjectHandle"></param>
		/// <returns></returns>
		[DllImport("advapi32.dll")]
		public static extern long LsaClose(IntPtr ObjectHandle);
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="status"></param>
		/// <returns></returns>
		[DllImport("advapi32.dll")]
		public static extern uint LsaNtStatusToWinError(uint status);

		/// <summary>
		/// 
		/// </summary>
		[StructLayout(LayoutKind.Sequential)]
		public struct LSA_UNICODE_STRING
		{
			public UInt16 Length;
			public UInt16 MaximumLength;
			public IntPtr Buffer;
		}

		/// <summary>
		/// 
		/// </summary>
		[StructLayout(LayoutKind.Sequential)]
		public struct LSA_OBJECT_ATTRIBUTES
		{
			public int Length;
			public IntPtr RootDirectory;
			public LSA_UNICODE_STRING ObjectName;
			public UInt32 Attributes;
			public IntPtr SecurityDescriptor;
			public IntPtr SecurityQualityOfService;
		}

		/// <summary>
		/// 
		/// </summary>
		[StructLayout(LayoutKind.Sequential)]
		public struct LUID
		{
			public readonly Int32 LowPart;
			public readonly UInt32 HighPart;
		}

		/// <summary>
		/// 
		/// </summary>
		[StructLayout(LayoutKind.Sequential)]
		public struct TOKEN_PRIVILEGES
		{
			public Int32 PrivilegeCount;
			public LUID Luid;
			public Int32 Attributes;
		}

		[DllImport("advapi32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool LookupPrivilegeValue(string lpsystemname, string lpname, [MarshalAs(UnmanagedType.Struct)] ref LUID lpLuid);

		[DllImport("advapi32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool AdjustTokenPrivileges(IntPtr tokenhandle,
			[MarshalAs(UnmanagedType.Bool)] bool disableAllPrivileges,
			[MarshalAs(UnmanagedType.Struct)]ref TOKEN_PRIVILEGES newstate,
			uint bufferlength, IntPtr previousState, IntPtr returnlength);

		public const int SE_PRIVILEGE_ENABLED = 0x00000002;

		public const int ERROR_NOT_ALL_ASSIGNED = 1300;

		public const UInt32 STANDARD_RIGHTS_REQUIRED = 0x000F0000;
		public const UInt32 STANDARD_RIGHTS_READ = 0x00020000;
		public const UInt32 TOKEN_ASSIGN_PRIMARY = 0x0001;
		public const UInt32 TOKEN_DUPLICATE = 0x0002;
		public const UInt32 TOKEN_IMPERSONATE = 0x0004;
		public const UInt32 TOKEN_QUERY = 0x0008;
		public const UInt32 TOKEN_QUERY_SOURCE = 0x0010;
		public const UInt32 TOKEN_ADJUST_PRIVILEGES = 0x0020;
		public const UInt32 TOKEN_ADJUST_GROUPS = 0x0040;
		public const UInt32 TOKEN_ADJUST_DEFAULT = 0x0080;
		public const UInt32 TOKEN_ADJUST_SESSIONID = 0x0100;
		public const UInt32 TOKEN_READ = (STANDARD_RIGHTS_READ | TOKEN_QUERY);
		public const UInt32 TOKEN_ALL_ACCESS = (STANDARD_RIGHTS_REQUIRED |
		                                          TOKEN_ASSIGN_PRIMARY |
		                                          TOKEN_DUPLICATE |
		                                          TOKEN_IMPERSONATE |
		                                          TOKEN_QUERY |
		                                          TOKEN_QUERY_SOURCE |
		                                          TOKEN_ADJUST_PRIVILEGES |
		                                          TOKEN_ADJUST_GROUPS |
		                                          TOKEN_ADJUST_DEFAULT |
		                                          TOKEN_ADJUST_SESSIONID);

		[DllImport("Advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool OpenProcessToken(IntPtr processHandle,
			uint desiredAccesss,
			out IntPtr tokenHandle);
	}
}
