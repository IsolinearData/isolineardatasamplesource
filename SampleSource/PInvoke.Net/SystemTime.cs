﻿using System;
using System.Runtime.InteropServices;

// ReSharper disable MemberCanBePrivate.Global

namespace PInvoke.Net
{
	[StructLayout(LayoutKind.Sequential, Pack = 2)]
	public struct SystemTime
	{
		#region Members

		/// <summary>
		/// 
		/// </summary>
		public readonly ushort Year;
		public readonly ushort Month;
		public readonly ushort DayOfWeek;
		public readonly ushort Day;
		public readonly ushort Hour;
		public readonly ushort Minute;
		public readonly ushort Second;
		public readonly ushort Milliseconds;

		#endregion

		#region Lifetime

		/// <summary>
		/// Ctor
		/// </summary>
		/// <param name="dt"></param>
		public SystemTime(DateTime dt)
		{
			dt = dt.ToUniversalTime();
			Year = Convert.ToUInt16(dt.Year);
			Month = Convert.ToUInt16(dt.Month);
			DayOfWeek = Convert.ToUInt16(dt.DayOfWeek);
			Day = Convert.ToUInt16(dt.Day);
			Hour = Convert.ToUInt16(dt.Hour);
			Minute = Convert.ToUInt16(dt.Minute);
			Second = Convert.ToUInt16(dt.Second);
			Milliseconds = Convert.ToUInt16(dt.Millisecond);
		}

		/// <summary>
		/// Ctor
		/// </summary>
		/// <param name="year"></param>
		/// <param name="month"></param>
		/// <param name="day"></param>
		/// <param name="hour"></param>
		/// <param name="minute"></param>
		/// <param name="second"></param>
		/// <param name="millisecond"></param>
		public SystemTime(ushort year, ushort month, ushort day, ushort hour = 0, ushort minute = 0, ushort second = 0, ushort millisecond = 0)
		{
			Year = year;
			Month = month;
			Day = day;
			Hour = hour;
			Minute = minute;
			Second = second;
			Milliseconds = millisecond;
			DayOfWeek = 0;
		}

		#endregion

		#region Conversion

		/// <summary>
		/// 
		/// </summary>
		/// <param name="st"></param>
		public static implicit operator DateTime(SystemTime st)
		{
			// sanity
			if (st.Year == 0 || st == MinValue)
			{
				return DateTime.MinValue;
			}

			return st == MaxValue ?
				DateTime.MaxValue :
				new DateTime(st.Year, st.Month, st.Day, st.Hour, st.Minute, st.Second, st.Milliseconds, DateTimeKind.Local);
		}

		#endregion

		#region Equality

		/// <summary>
		/// 
		/// </summary>
		/// <param name="s1"></param>
		/// <param name="s2"></param>
		/// <returns></returns>
		public static bool operator ==(SystemTime s1, SystemTime s2)
		{
			return (s1.Year == s2.Year && s1.Month == s2.Month && s1.Day == s2.Day && s1.Hour == s2.Hour &&
					s1.Minute == s2.Minute && s1.Second == s2.Second && s1.Milliseconds == s2.Milliseconds);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="s1"></param>
		/// <param name="s2"></param>
		/// <returns></returns>
		public static bool operator !=(SystemTime s1, SystemTime s2)
		{
			return !(s1 == s2);
		}

		#endregion

		#region Static

		/// <summary>
		/// 
		/// </summary>
		public static readonly SystemTime MinValue, MaxValue;

		/// <summary>
		/// 
		/// </summary>
		static SystemTime()
		{
			MinValue = new SystemTime(1601, 1, 1);
			MaxValue = new SystemTime(30827, 12, 31, 23, 59, 59, 999);
		}

		#endregion

		#region Overrides

		/// <summary>
		/// 
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public override bool Equals(object obj)
		{
			if (obj is SystemTime)
				return ((SystemTime)obj) == this;
			return base.Equals(obj);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		#endregion
	}
}
