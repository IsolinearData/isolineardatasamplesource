﻿using System;
using System.Runtime.InteropServices;

namespace PInvoke.Net
{
	public static class Kernel32
	{
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		[DllImport("kernel32.dll")]
		public static extern int GetLastError();

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		[DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		public static extern IntPtr GetCurrentProcess();

		/// <summary>
		/// 
		/// </summary>
		/// <param name="hObject"></param>
		/// <returns></returns>
		[DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool CloseHandle(IntPtr hObject);

		/// <summary>
		/// Sets the current system time and date. The system time is expressed in Coordinated Universal Time (UTC).
		/// </summary>
		/// <param name="sysTime">A pointer to a SYSTEMTIME structure that contains the new system date and time.
		/// The wDayOfWeek member of the SYSTEMTIME structure is ignored.</param>
		/// <returns>
		/// If the function succeeds, the return value is nonzero.
		///
		/// If the function fails, the return value is zero.To get extended error information, call GetLastError.
		/// </returns>
		[DllImport("kernel32.dll", EntryPoint = "SetSystemTime", SetLastError = true)]
		public static extern bool SetSystemTime(ref SystemTime sysTime);
	}
}
