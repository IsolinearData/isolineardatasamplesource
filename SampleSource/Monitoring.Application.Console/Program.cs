﻿
using IDI.Business.Constants;
using IDI.Common;

using System;

namespace Monitoring.Application
{
	internal static class Program
	{
		/// <summary>
		/// Mutex to signal that the application is closing, restart the application again after exit
		/// </summary>
		private static SignalProcess terminate;

		// ReSharper disable once UnusedParameter.Local
		private static void Main(string[] args)
		{
			Console.WriteLine("Monitoring WPF Signalling Application");

			// initialize signalling object
			terminate = new SignalProcess(AppConstants.UniqueID_Terminate);
			terminate.Init(false);
			terminate.Handle.WaitOne();

			Console.WriteLine("WPF Signalling Application terminated!");
			Console.ReadLine();
		}
	}
}
