﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDI.Business.Constants
{
	public static class AppConstants
	{
		/// <summary>
		/// some unique stirng in the known universe
		/// </summary>
		// ReSharper disable once InconsistentNaming
		public const string UniqueID_Terminate = "Signalling-Application-Wpf-{AE8644C2-36D9-4B7E-8437-5B8A609B4A93}-Terminiate";
	}
}
