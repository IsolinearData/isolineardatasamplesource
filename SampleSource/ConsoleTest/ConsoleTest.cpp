// ConsoleTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

union myUnion
{
	unsigned int info;
	struct
	{
		unsigned int flag1 : 1; // bit 0
		unsigned int flag2 : 1; // bit 1
		unsigned int flag3 : 1; // bit 2
		unsigned int flag4 : 1; // bit 3
		unsigned int flag5 : 1; // bit 4
		unsigned int flag6 : 1; // bit 5
		//	.
		//	.
		//	.
		unsigned int flag31 : 1; // bit 31
	};
};

union myUnion2
{
	unsigned int info;
	struct
	{
		unsigned int flag1 : 3; // bit 0-2
		unsigned int flag2 : 1; // bit 3
		unsigned int flag3 : 4; // bit 4-7
		unsigned int flag4 : 1; // bit 8
		//	.
		//	.
		//	.
		unsigned int flag31 : 1; // bit 31
	};
};




int main()
{
    return 0;
}

