﻿using System;

// ReSharper disable once CheckNamespace
namespace IDI.Common
{
	/// <summary>
	/// Helper class providing functionality to support managing object lifetime
	/// </summary>
	public static class Lifetime
	{
		/// <summary>
		/// Dispose an object of type 'T' that *doesn't* not explicitly expose a public 'Dispose' method, but does
		/// implement an IDisposable interface and implementation.
		/// </summary>
		/// <typeparam name="T">Object's type</typeparam>
		/// <param name="obj">Object who's lifetime we want to manage</param>
		public static void DisposeIt<T>(ref T obj) where T : IDisposable
		{
			// sanity
			if (obj == null)
			{
				return;
			}

			// dispose object
			var disposable = obj as IDisposable;
			disposable.Dispose();
			obj = default(T);
		}

		/// <summary>
		/// Dispose an object of type 'T' that *doesn't* not explicitly expose a public 'Dispose' method, but does
		/// implement an IDisposable interface and implementation.
		/// In addition, allows caller in add custom cleanup logic before Dispose() is called.
		/// </summary>
		/// <typeparam name="T">Object's type</typeparam>
		/// <param name="obj">Object who's lifetime we want to manage</param>
		/// <param name="handler">client implemented custom cleanup handler</param>
		public static void DisposeIt<T>(ref T obj, Action<T> handler) where T : IDisposable
		{
			// sanity
			if (obj == null)
			{
				return;
			}

			// call custom handler if defined
			handler?.Invoke(obj);

			// dispose object
			var disposable = obj as IDisposable;
			disposable.Dispose();
			obj = default(T);
		}

		/// <summary>
		/// Dispose an object of type 'T' that *does* explicitly expose a public 'Dispose' method.
		/// It will usually also implement an IDisposable interface and implementation.
		/// </summary>
		/// <typeparam name="T">Object's type</typeparam>
		/// <param name="obj">Object who's lifetime we want to manage</param>
		public static void DisposeItExplicit<T>(ref T obj) where T : IDisposable
		{
			// sanity
			if (obj == null)
			{
				return;
			}

			// dispose object
			obj.Dispose();
			obj = default(T);
		}
	}
}
