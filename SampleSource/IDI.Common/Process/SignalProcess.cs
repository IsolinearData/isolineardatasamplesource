﻿using System;
using System.Threading;
using System.Threading.Tasks;

// ReSharper disable once CheckNamespace
namespace IDI.Common
{
	public class SignalProcess : IDisposable
	{
		#region Instance

		/// <summary>
		/// Kernel object used to synchronize beyween the ABS NG application and the process monitor application
		/// </summary>
		/// <remarks>
		/// Usage:
		/// 
		///	 // wrap the mutexs
		///  stdExit = new SignalProcess(AppConstants.UniqueID_StdExit);
		///  stdExit.InitAndWait();
		///
		///  terminate = new SignalProcess(AppConstants.UniqueID_Terminate);
		///  terminate.InitAndWait();
		/// </remarks>
		private Mutex monitorApp;

		/// <summary>
		/// Event used to signal to release the mutex on the same thread used to create the mutex
		/// </summary>
		private ManualResetEvent releaseMutex = new ManualResetEvent(false);

		/// <summary>
		/// Event used to signal to just clean everything up
		/// </summary>
		private ManualResetEvent cleanupSignal = new ManualResetEvent(false);

		#endregion

		#region Properties

		/// <summary>
		/// Mutex unique ID
		/// </summary>
		// ReSharper disable once InconsistentNaming
		// ReSharper disable once AutoPropertyCanBeMadeGetOnly.Local
		private string UniqueID { get; set; }

		/// <summary>
		/// wait handle for this signal object
		/// </summary>
		public WaitHandle Handle => monitorApp;

		#endregion

		#region Lifetime

		/// <summary>
		/// Ctor
		/// </summary>
		/// <param name="uniqueID"></param>
		// ReSharper disable once InconsistentNaming
		public SignalProcess(string uniqueID)
		{
			UniqueID = uniqueID;
		}

		/// <summary>
		/// Intialize
		/// </summary>
		/// <param name="initiallyOwned"></param>
		/// <returns></returns>
		// ReSharper disable once UnusedMethodReturnValue.Local
		public bool Init(bool initiallyOwned = false)
		{
			// Build unique application Id and the IPC channel name.
			var applicationIdentifierTerminate = UniqueID + Environment.UserName;

			// Create mutex based on unique application ID to check if this is the first instance of the application. 
			bool firstInstanceTerminate;
			monitorApp = new Mutex(initiallyOwned, applicationIdentifierTerminate, out firstInstanceTerminate);
			return firstInstanceTerminate;
		}

		/// <summary>
		/// Initialize the mutex and wait.
		/// This allows us to signal the mutex on the same thread used to create the mutex.
		/// </summary>
		/// <param name="initiallyOwned"></param>
		public async void InitAndWait(bool initiallyOwned = true)
		{
			Init(initiallyOwned);

			var which = await WaitAsync();
			// ReSharper disable once SwitchStatementMissingSomeCases
			switch (which)
			{
				case 0:
					monitorApp?.ReleaseMutex();
					break;
				case 1:
					break;
			}
		}

		/// <summary>
		/// Allows us to wrap the WaitAny() inside of an awaitable task
		/// </summary>
		/// <returns>which handle satisfied the Wait()</returns>
		private async Task<int> WaitAsync()
		{
			WaitHandle[] handles = { releaseMutex, cleanupSignal };
			var which = await Task.Factory.StartNew(() => WaitHandle.WaitAny(handles, Timeout.Infinite));
			return which;
		}

		#endregion

		#region Operations

		/// <summary>
		/// Signal the mutex via it's event object
		/// </summary>
		public void Signal()
		{
			releaseMutex.Set();
		}

		/// <summary>
		/// Signal the mutex directly
		/// </summary>
		public void Release()
		{
			monitorApp.ReleaseMutex();
		}

		/// <summary>
		/// Signal cleanup
		/// </summary>
		public void Cleanup()
		{
			cleanupSignal.Set();
		}

		#endregion

		#region IDisposable

		/// <summary>
		/// Cleanup kernel objects
		/// </summary>
		private void ReleaseUnmanagedResources()
		{
			// cleanup
			Lifetime.DisposeIt(ref monitorApp);
			Lifetime.DisposeIt(ref releaseMutex);
			Lifetime.DisposeIt(ref cleanupSignal);
		}

		/// <inheritdoc />
		/// <summary>
		/// Dispose managed resources
		/// </summary>
		public void Dispose()
		{
			ReleaseUnmanagedResources();
			GC.SuppressFinalize(this);
		}

		/// <summary>
		/// Finalizer
		/// </summary>
		~SignalProcess()
		{
			ReleaseUnmanagedResources();
		}

		#endregion
	}
}
