﻿using Microsoft.Win32.SafeHandles;

using System;
using System.Diagnostics;
using System.Threading;

// ReSharper disable once CheckNamespace
namespace IDI.Common
{
	public class MonitorProcessExit : IDisposable
	{
		#region Instance
		/// <summary>
		/// 
		/// </summary>
		private SafeWaitHandle safeProcessTerminated;

		/// <summary>
		/// 
		/// </summary>
		private ManualResetEvent processTerminated;

		#endregion

		#region  Properties

		/// <summary>
		/// 
		/// </summary>
		public WaitHandle Handle => processTerminated;

		#endregion

		#region Operations

		/// <summary>
		/// 
		/// </summary>
		/// <param name="process"></param>
		/// <returns></returns>
		public SafeWaitHandle WrapProcessHandle(Process process)
		{
			if (process == null)
			{
				return null;
			}
			safeProcessTerminated = new SafeWaitHandle(process.Handle, false);
			processTerminated = new ManualResetEvent(true)
			{
				SafeWaitHandle = safeProcessTerminated
			};
			return safeProcessTerminated;
		}

		#endregion

		#region IDisposable

		/// <summary>
		/// To detect redundant calls
		/// </summary>
		private bool isAlreadyDisposed;

		/// <summary>
		/// Finalizer is required to handle cases where Dispose is not explicitly called
		/// </summary>
		~MonitorProcessExit()
		{
			Dispose(false);
		}

		/// <summary>
		/// Dual handler for both the public interface and the finalizer
		/// </summary>
		/// <param name="disposing"></param>
		private void Dispose(bool disposing)
		{
			if (isAlreadyDisposed)
			{
				return;
			}

			if (disposing)
			{
				Lifetime.DisposeIt(ref processTerminated);
				Lifetime.DisposeIt(ref safeProcessTerminated);
			}

			isAlreadyDisposed = true;
		}

		/// <inheritdoc />
		/// <summary>
		/// Public garbage collection interface
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		#endregion
	}
}
