﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;
using PInvoke.Net;

namespace IDI.Common
{
	public static class LsaUtility
	{
		// enum all policies

		[Flags]
		private enum LSA_AccessPolicy : long
		{
			POLICY_VIEW_LOCAL_INFORMATION = 0x00000001L,
			POLICY_VIEW_AUDIT_INFORMATION = 0x00000002L,
			POLICY_GET_PRIVATE_INFORMATION = 0x00000004L,
			POLICY_TRUST_ADMIN = 0x00000008L,
			POLICY_CREATE_ACCOUNT = 0x00000010L,
			POLICY_CREATE_SECRET = 0x00000020L,
			POLICY_CREATE_PRIVILEGE = 0x00000040L,
			POLICY_SET_DEFAULT_QUOTA_LIMITS = 0x00000080L,
			POLICY_SET_AUDIT_REQUIREMENTS = 0x00000100L,
			POLICY_AUDIT_LOG_ADMIN = 0x00000200L,
			POLICY_SERVER_ADMIN = 0x00000400L,
			POLICY_LOOKUP_NAMES = 0x00000800L,
			POLICY_NOTIFICATION = 0x00001000L
		}

		/// <summary>Adds a privilege to an account</summary>
		/// <param name="accountName">Name of an account - "domain\account" or only "account"</param>
		/// <param name="privilegeName">Name ofthe privilege</param>
		/// <returns>The windows error code returned by LsaAddAccountRights</returns>
		public static long SetRight(string accountName, string privilegeName)
		{
			// contains the last error
			uint winErrorCode;

			// pointer an size for the SID
			var sid = IntPtr.Zero;
			var sidSize = 0;

			// StringBuilder and size for the domain name
			var domainName = new StringBuilder();
			var nameSize = 0;

			// account-type variable for lookup
			var accountType = 0;

			// get required buffer size
			AdvApi32.LookupAccountName(string.Empty, accountName, sid, ref sidSize, domainName, ref nameSize, ref accountType);

			// allocate buffers
			domainName = new StringBuilder(nameSize);
			sid = Marshal.AllocHGlobal(sidSize);

			// lookup the SID for the account
			var result = AdvApi32.LookupAccountName(string.Empty, accountName, sid, ref sidSize, domainName, ref nameSize, ref accountType);

			// say what you're doing
			Debug.WriteLine($"LookupAccountName result = {result}");
			Debug.WriteLine($"IsValidSid: {AdvApi32.IsValidSid(sid)}");
			Debug.WriteLine($"LookupAccountName domainName: {domainName}");

			if (!result)
			{
				winErrorCode = (uint)Kernel32.GetLastError();
				Debug.WriteLine($"LookupAccountName failed: {winErrorCode}");
			}
			else
			{
				//initialize an empty unicode-string
				var systemName = new AdvApi32.LSA_UNICODE_STRING();

				//combine all policies
				const int access = (int)(
					LSA_AccessPolicy.POLICY_AUDIT_LOG_ADMIN |
					LSA_AccessPolicy.POLICY_CREATE_ACCOUNT |
					LSA_AccessPolicy.POLICY_CREATE_PRIVILEGE |
					LSA_AccessPolicy.POLICY_CREATE_SECRET |
					LSA_AccessPolicy.POLICY_GET_PRIVATE_INFORMATION |
					LSA_AccessPolicy.POLICY_LOOKUP_NAMES |
					LSA_AccessPolicy.POLICY_NOTIFICATION |
					LSA_AccessPolicy.POLICY_SERVER_ADMIN |
					LSA_AccessPolicy.POLICY_SET_AUDIT_REQUIREMENTS |
					LSA_AccessPolicy.POLICY_SET_DEFAULT_QUOTA_LIMITS |
					LSA_AccessPolicy.POLICY_TRUST_ADMIN |
					LSA_AccessPolicy.POLICY_VIEW_AUDIT_INFORMATION |
					LSA_AccessPolicy.POLICY_VIEW_LOCAL_INFORMATION
				);

				// initialize a pointer for the policy handle
				// ReSharper disable once RedundantAssignment
				var policyHandle = IntPtr.Zero;

				// these attributes are not used, but LsaOpenPolicy wants them to exists
				var ObjectAttributes = new AdvApi32.LSA_OBJECT_ATTRIBUTES
				{
					Length = 0,
					RootDirectory = IntPtr.Zero,
					Attributes = 0,
					SecurityDescriptor = IntPtr.Zero,
					SecurityQualityOfService = IntPtr.Zero
				};

				// get a policy handle
				var resultPolicy = AdvApi32.LsaOpenPolicy(ref systemName, ref ObjectAttributes, access, out policyHandle);
				winErrorCode = AdvApi32.LsaNtStatusToWinError(resultPolicy);

				if (winErrorCode != 0)
				{
					Debug.WriteLine($"OpenPolicy failed: {winErrorCode}");
				}
				else
				{
					// Now that we have the SID an the policy,
					// we can add rights to the account.

					// initialize an unicode-string for the privilege name
					var userRights = new AdvApi32.LSA_UNICODE_STRING[1];
					userRights[0] = new AdvApi32.LSA_UNICODE_STRING
					{
						Buffer = Marshal.StringToHGlobalUni(privilegeName),
						Length = (UInt16)(privilegeName.Length * UnicodeEncoding.CharSize),
						MaximumLength = (UInt16)((privilegeName.Length + 1) * UnicodeEncoding.CharSize)
					};

					//a dd the right to the account
					var res = AdvApi32.LsaAddAccountRights(policyHandle, sid, userRights, 1);
					winErrorCode = AdvApi32.LsaNtStatusToWinError(res);
					if (winErrorCode != 0)
					{
						Debug.WriteLine($"LsaAddAccountRights failed: {winErrorCode}");
					}

					AdvApi32.LsaClose(policyHandle);
				}

				AdvApi32.FreeSid(sid);
			}

			return winErrorCode;
		}

	}
}
